checkInterfaceStatus = async function(sudoPW, funcCalledFrom) {
    try {
        if(sudoPW == "" || sudoPW == null) {
            // console.log("No sudo password available yet.");
            $("#authModal").modal('open');
        } else {
            // console.log("Running VPN check now.");
            let myVPNs = await Neutralino.os.execCommand("echo " + sudoPW + " | sudo -S wg show");
            if (typeof myVPNs != 'undefined') {
                // console.log(myVPNs.stdOut);
                let myVPNsArray = (myVPNs.stdOut).split('\n');
                let noArray = myVPNsArray.length;
                for (i=0; i < noArray; i++) {
                    let arrPart = myVPNsArray[i].split(': ');
                    // console.log("i = " + i + " - noArray = " + noArray);
                    if (arrPart[0] == 'interface') {
                        if (funcCalledFrom == "askEtcWireguard") {
                            // console.log("Interface is up - name is: " + arrPart[1]);
                            let intName = "con|" + arrPart[1];
                            document.getElementById(intName).checked = true;
                            repeatConnCheck(sudoPW, arrPart[1]);
                        } else if (funcCalledFrom == "disconnectOnClose") {
                            // console.log("Trying to disconnect " + arrPart[1]);
                            let discon = await Neutralino.os.execCommand("echo " + sudoPW + " | sudo -S wg-quick down " + arrPart[1]);
                        }
                    }
                    if (i ==  (noArray -1) && funcCalledFrom == "disconnectOnClose") {
                        // console.log("Calling Exit App Funcation!");
                        exitAppNow();
                    }
                }
            }
        }
    } catch (error) {
        console.log("   ERROR trying to read any current WG connections: ");
        console.dir(error);
    }
}


// set some default values on app start for data rates
let rcvdHist;
let sentHist;
let rcvd;
let sent;
let rcvdNo;
let sentNo;
let rcvdLabel = '';
let sentLabel = '';

checkConnection = async function(sudoPW, intName) {
    rcvdHist = rcvd;
    sentHist = sent;
    let myVPNs = await Neutralino.os.execCommand("echo " + sudoPW + " | sudo -S wg show " + intName + " transfer");
    // check that we get a return value from the command above
    if (typeof myVPNs != 'undefined') {
        // split the return value into separete lines in an array
        let myVPNArray = (myVPNs.stdOut).split('\t');

        // handle simply showing total trans / recv
        let showTransInfo = await Neutralino.filesystem.readFile(storageDir + '/appSettings/appSettings.conf');
        let showTransInfoObj = JSON.parse(showTransInfo);

        // check settings to see how we should display data transfer info 
        if (showTransInfoObj.show_time_based_transfer_rates == false) {
            // show tht total data transfered over the connection
            rcvd = myVPNArray[1];
            sent = myVPNArray[2];

            rcvdNo = Number(rcvd);
            sentNo = Number(sent);

            let rcvdTest = rcvdNo / 1000;
            let sentTest = sentNo / 1000;
            
            // figure out which label to use for received total
            if (rcvdTest < 1 || isNaN(rcvdTest)) {
                rcvdNo = rcvdTest.toFixed(2);
                rcvdLabel = "B";
            } else {
                let rcvdTest2 = rcvdNo / 1000000;
                if (rcvdTest2 < 1) {
                    rcvdNo = rcvdTest.toFixed(2);
                    rcvdLabel = "kB";
                } else {
                    let rcvdTest3 = rcvdNo / 1000000000;
                    if (rcvdTest3 < 1) {
                        rcvdNo = rcvdTest2.toFixed(2);
                        rcvdLabel = "MB";
                    } else {
                        rcvdNo = rcvdTest3.toFixed(2);
                        rcvdLabel = "GB";
                    }
                }
            }

            // figure out which label to use for sent total
            if (sentTest < 1 || isNaN(sentTest)) {
                sentNo = sentTest.toFixed(2);
                sentLabel = "B";
            } else {
                let sentTest2 = sentNo / 1000000;
                if (sentTest2 < 1) {
                    sentNo = sentTest.toFixed(2);
                    sentLabel = "kB";
                } else {
                    let sentTest3 = sentNo / 1000000000;
                    if (sentTest3 < 1) {
                        sentNo = sentTest2.toFixed(2);
                        sentLabel = "MB";
                    } else {
                        sentNo = sentTest3.toFixed(2);
                        sentLabel = "GB";
                    }
                }
            }
        } else {
            // show the data transfer rate (data / second) averaged every 5 seconds
            rcvd = myVPNArray[1];
            sent = myVPNArray[2];

            let adjRcvdNo = Number(rcvd);
            let adjSentNo = Number(sent);
            let rcvdHistNo = Number(rcvdHist);
            let sentHistNo = Number(sentHist);

            rcvdNo = (adjRcvdNo - rcvdHistNo) / 5;
            sentNo = (adjSentNo - sentHistNo) / 5;

            let rcvdTest = rcvdNo / 1000;
            let sentTest = sentNo / 1000;

            // figure out which label to use for receive rate
            if (rcvdTest < 1 || isNaN(rcvdTest)) {
                rcvdNo = rcvdTest.toFixed(2);
                rcvdLabel = "B/s";
            } else {
                let rcvdTest2 = rcvdNo / 1000000;
                if (rcvdTest2 < 1) {
                    rcvdNo = rcvdTest.toFixed(2);
                    rcvdLabel = "kB/s";
                } else {
                    let rcvdTest3 = rcvdNo / 1000000000;
                    if (rcvdTest3 < 1) {
                        rcvdNo = rcvdTest2.toFixed(2);
                        rcvdLabel = "MB/s";
                    } else {
                        rcvdNo = rcvdTest3.toFixed(2);
                        rcvdLabel = "GB/s";
                    }
                }
            }

            // figure out which label to use for sent rate
            if (sentTest < 1 || isNaN(sentTest)) {
                sentNo = sentTest.toFixed(2);
                sentLabel = "B/s";
            } else {
                let sentTest2 = sentNo / 1000000;
                if (sentTest2 < 1) {
                    sentNo = sentTest.toFixed(2);
                    sentLabel = "kB/s";
                } else {
                    let sentTest3 = sentNo / 1000000000;
                    if (sentTest3 < 1) {
                        sentNo = sentTest2.toFixed(2);
                        sentLabel = "MB/s";
                    } else {
                        sentNo = sentTest3.toFixed(2);
                        sentLabel = "GB/s";
                    }
                }
            }
        }

        // display our results in our html page.
        $("#spd_" + intName).empty();
        $("#spd_" + intName).append(`
            <div class="col s3">
                <strong>Recvd:</strong>
            </div>
            <div class="col s3">` + rcvdNo + ` ` + rcvdLabel + `</div>
            <div class="col s3">
                <strong>Sent:</strong>
            </div>
            <div class="col s3">` + sentNo + ` ` + sentLabel + `</div>
        `)
    }
}

// start our status check repeated every 5 seconds (t000 milliseconds)
repeatConnCheck = function(sudoPW, intName) {
    checkSpeedInterval = setInterval(function() {
        checkConnection(sudoPW, intName);
    }, 5000);
}

// stop our status check
stopRepeatConnCheck = function() {
    clearInterval(checkSpeedInterval)
}