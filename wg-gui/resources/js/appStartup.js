var mysudopw;
var fullConfNameArr = [];
var ifaceName = {};
var storageDir;
var setDisOnClose=false;
var checkSpeedInterval;

$("document").ready(function() {
    $('.modal').modal();
    $('.collapsible').collapsible();
    $('.sidenav').sidenav();

    checkAppStoreExists = async function() {
        try {
            let myHome = await Neutralino.os.execCommand('echo $HOME');
            let myHomeTrim = myHome.stdOut.trim();
            // console.log(myHome.stdOut);
            storageDir = myHomeTrim + '/.local/share/appStorage';
            let exists = await Neutralino.filesystem.readDirectory(storageDir);
            // console.log("It exists!");
            importExistingConfigs();
        } catch (error) {
            console.log("    INFO: App Storage not found - Creating it...");
            mkAppStorage();
        }
    }
    checkAppStoreExists();

    mkAppStorage = async function() {   
        try {
            let data = await Neutralino.filesystem.createDirectory(storageDir);
            let addDir = await Neutralino.filesystem.createDirectory(storageDir + '/configLists');
            let addSettingsDir = await Neutralino.filesystem.createDirectory(storageDir + '/appSettings');
            let setInitialSettings = await Neutralino.filesystem.writeFile(storageDir + '/appSettings/appSettings.conf', '{"disconnect_on_close":false, "import_from_etc_wireguard_asked":false, "show_time_based_transfer_rates":false}');
        } catch (error) {
            console.log("Error trying to create the App Storage folder.");
            console.dir(error);
        }
    }

    importExistingConfigs = async function() {
        try {
            const currConfs = await Neutralino.filesystem.readDirectory(storageDir);
            // console.dir(currConfs);
            addConfsToList(currConfs);
        } catch (error) {
            console.log("Error trying to read the appStorage directory: ");
            console.dir(error);
        }
    }

    addConfsToList = async function(currConfs) {
        try {
            let conflist = "";
            var confArr = [];
            let noConfs = currConfs.length;
            // console.log("noConfs: " + noConfs);
            for (i=0; i < noConfs; i++) {
                if (currConfs[i].type == "FILE") {
                    fullConfNameArr.push(currConfs[i].entry);
                    currConfNamePart = (currConfs[i].entry).split('.');
                    if (currConfNamePart[1] == 'conf') {
                        currConfName = currConfNamePart[0];
                        // console.log("Adding " + currConfName + " to list.");
                        conflist = conflist.concat(currConfName + '\n');
                        confArr.push(currConfName);
                    } else {
                        // console.log("File " + currConfs[i].entry + " did not appear to be a wireguard configuration file.");
                    }
                }
                if (i == noConfs - 1) {
                    // console.dir("Moving to createConfList");
                    createConfsList(conflist, confArr);
                }
            }
        } catch (error) {
            console.log("    ERROR adding configuraions to list on startup:")
            console.dir(error);
        }
    }

    createConfsList = async function(confList, confArr) {
        try {
            // console.log("adding to configuration list.");
            let listAdd = await Neutralino.filesystem.writeFile(storageDir +'/configLists/confList.txt', confList);
            getMoreConfData(confArr);
        } catch (error) {
            console.log("Error adding the list of configs to a file:");
            console.dir(error);
        }
    }

    getMoreConfData = async function(confArr) {
        try {
            let namelength = confArr.length;
            if (namelength > 0) {
                for (i=0; i < namelength; i++) {
                    let confInfo = await Neutralino.filesystem.readFile(storageDir + '/' + fullConfNameArr[i]);
                    createConfDetail(confArr[i], confInfo);
                    if (i == (namelength - 1)) {
                        updateUIWithConfs(confArr, ifaceName);
                    }
                }
            } else {
                updateUIWithConfs(confArr, ifaceName)
            }
        } catch (error) {
            console.log("Error reading the configuration files for more detail: ");
            console.dir(error);
        }
    }

    createConfDetail = function(confName, confInfo) {
    var ifaceData = {};
    let lines = confInfo.split('\n');
    for (j=0; j < lines.length; j++) {
        let lineInfo = lines[j].split(' = ');
        if (typeof lineInfo[0]== 'undefined' || typeof lineInfo[1] == 'undefined') {
            // console.log("got undefined line information.");
        } else {
            ifaceData[lineInfo[0]] = lineInfo[1];
        }
        if (j == lines.length - 1) {
            ifaceName[confName] = ifaceData;
        }
    }
        // console.log("    -----------------------    ");
        // console.dir(ifaceName);
    }

    updateUIWithConfs = function(confArr, ifaceName) {
        try {
            // console.log("Got to the UI builder.");
            // first let's empty our div
            $("#configListItems").empty();
            for (i=0; i < confArr.length; i++) {
                let thisIFace = confArr[i];
                $("#configListItems").append(`
                <li theme>
                    <div class="collapsible-header">
                    ` + confArr[i] + `
                    <div class="switch right">
                        <label>
                        Connect
                        <input type="checkbox" id="con|` + confArr[i] + `" onclick="connect(this.id)">
                        <span class="lever"></span>
                        </label>
                    </div>
                    </div>
                    <div class="collapsible-body">
                        <div class="row">
                            <div class="col s4">
                                <strong>IPv4</strong>
                            </div>
                            <div class="col s8">
                                ` +ifaceName[thisIFace].Address + `
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s4">
                                <strong>Endpoint</strong>
                            </div>
                            <div class="col s8">
                                ` +ifaceName[thisIFace].Endpoint + `
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s4">
                                <strong>Allowed IPs</strong>
                            </div>
                            <div class="col s8">
                                ` + ifaceName[thisIFace].AllowedIPs + `
                            </div>
                        </div>
                        <div class="row" id="spd_` + confArr[i] + `"></div>
                    </div>
                </li>`);
            }
            // console.log("About to call theme check on startup.");
            checkTheme();
        } catch (error) {
            console.log("    ERROR building the user interface in html:")
            console.dir(error);
        }
    }

    getPassword();
});