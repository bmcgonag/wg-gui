authenticateAction.onclick = function() {
    getPassword();
}

// The code below is for the modal to get sudo password
getPassword = function() {
    $("#authModal").modal('open');
}

// accept the 'Enter' key as a click when entering the sudo password
let pwinput = document.getElementById('sudopw');
pwinput.addEventListener("keypress", function(event) {
    if (event.key === "Enter") {
        document.getElementById("authBtn").click();
    }
}); 

storeAuth = function() {
    mysudopw = $("#sudopw").val();
    // console.log("call interface status check.");
    askEtcWireguard(mysudopw);
    // checkInterfaceStatus(mysudopw, "storeAuth");
}