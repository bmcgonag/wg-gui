readDir = async function(myPath) {
    try {
        console.log("reached readDir function with path: " + myPath);
        let entries = await Neutralino.filesystem.readDirectory(myPath);
        let noEnt = entries.length;
        let confData = "Directory:" + myPath;
        for (i=0; i<noEnt; i++) {
            if (entries[i].type == "FILE") {
                currConfNamePart = (entries[i].entry).split('.');
                if (currConfNamePart[1] == 'conf') {
                    let copyFile = await Neutralino.filesystem.copyFile(myPath + '/' + entries[i].entry, storageDir + '/' + entries[i].entry);
                } else {
                    // console.log("File " + entries[i].entry + " did not appear to be a configuration file.");
                }  
            }
            if (i == (noEnt - 1)) {
              importExistingConfigs();
            }
        }
    } catch (error) {
        console.log("Promise likely rejected trying to read the directory chosen: ");
        console.dir(error);
    }
}

openDirPicker.onclick = async function() {
    try {
        $("#slide-out").sidenav('close');
        let entry = await Neutralino.os.showFolderDialog('Select Wireguard Config Directory:');
        readDir(entry);
    } catch (error) {
        console.log("Error trying to open the directory picker: ");
        console.dir(error);
    }
}