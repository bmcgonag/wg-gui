settings.onclick = function() {
    $("#settingsModal").modal('open');
    checkSettings();
}

let checkSettings = async function() {
    try {
        let mySettings = await Neutralino.filesystem.readFile(storageDir + '/appSettings/appSettings.conf');
        if (mySettings != null && mySettings != "") {
            let settingsObj = JSON.parse(mySettings);
            if (settingsObj.disconnect_on_close == true) {
                document.getElementById('disconnectOnClose').checked = true;
            }

            if (settingsObj.hide_on_close == true) {
                document.getElementById('hideOnClose').checked = true;
            }

            if (settingsObj.show_time_based_transfer_rates == true) {
                document.getElementById('showRate').checked = true;
            }
        }
    } catch (error) {
        console.log("    ERROR checking the settings file.");
        console.dir(error);
    }
}

// save the disconnect on close preference
let storeSettingsDisOnClose = function() {
    try {
        let dis_on_quit = $("#disconnectOnClose").prop('checked');
        saveSettings("disconnect_on_close", dis_on_quit);
    } catch (error) {
        console.log("    ERROR saving the settings file.");
        console.dir(error);
    }
}


// save the hide to tray preference
let storeSettingsHideToTray = function() {
    try {
        let hide_to_tray = $("#hideOnClose").prop('checked');
        saveSettings("hide_on_close", hide_to_tray);
    } catch (error) {
        console.log("    ERROR saving hide on close setting:");
        console.dir("error");
    }
}

let storeSettingsShowTransfer = function() {
    try {
        let show_time_based_transfer_rates = $("#showRate").prop('checked');
        saveSettings("show_time_based_transfer_rates", show_time_based_transfer_rates);
    } catch (error) {
        console.log("    ERROR saving show transfer rates setting:");
        console.dir("error");
    }
}

// perform the actual save to the appSettings.conf file
let saveSettings = async function(settingsKey, settingsValue) {
    try {
        let mySettings = await Neutralino.filesystem.readFile(storageDir + '/appSettings/appSettings.conf');
        if (mySettings == null || mySettings == "") {
            let settingsObj = {};
            // create our settings object
            settingsObj[settingsKey] = settingsValue;

            console.log(settingsKey, settingsValue);

            // turn our object into a string for storage
            let settingsObjStr = JSON.stringify(settingsObj);

            // store our settings
            let setSettings = await Neutralino.filesystem.writeFile(storageDir + '/appSettings/appSettings.conf', settingsObjStr);
            
            // show our success message
            showToast("Settings Saved!");
        } else {
            // turn our string into a json object
            let settingsObj = JSON.parse(mySettings);

            console.log(settingsKey, settingsValue);
            
            // add our new values to the existing object
            settingsObj[settingsKey] = settingsValue;

            // turn our object into a string for storage
            let newSettingsObjStr = JSON.stringify(settingsObj);

            // store our settings
            let setSettings = await Neutralino.filesystem.writeFile(storageDir + '/appSettings/appSettings.conf', newSettingsObjStr);
            
            // show our success message
            showToast("Settings Updated!"); 
        }   
    } catch (error) {
        console.log("    ERROR saving the settings file.");
        console.dir(error);
    }
}

// toggle the light or dark theme when the option is clicked
document.querySelector('.dmode').onclick = function() {
    let theme = document.querySelectorAll('.theme');
    let myMode = document.querySelector('.myMode');
    for (i=0; i < theme.length; i++) {
        theme[i].classList.toggle('grey');
        theme[i].classList.toggle('darken-4');
        theme[i].classList.toggle('white-text');
    }

    let colHead = document.querySelectorAll('.collapsible-header');
    for (j=0; j < colHead.length; j++) {
        colHead[j].classList.toggle('dark');
    }

    if (myMode.classList.length > 4) {
        saveSettings("theme", "dark");
    } else {
        saveSettings("theme", "light");
    }
    myMode.classList.toggle('far');
    myMode.classList.toggle('fa-moon');
}

// check the theme preference in the settings when the app starts
checkTheme = async function() {
    try {
        let mySettings = await Neutralino.filesystem.readFile(storageDir + '/appSettings/appSettings.conf');
        if (mySettings != null && mySettings != "") {
            let settingsObj = JSON.parse(mySettings);
            let themeApply = settingsObj.theme;
            if (themeApply == "dark") {
                let theme = document.querySelectorAll('.theme');
                let myMode = document.querySelector('.myMode');
                for (i=0; i < theme.length; i++) {
                    theme[i].classList.toggle('grey');
                    theme[i].classList.toggle('darken-4');
                    theme[i].classList.toggle('white-text');
                }

                let colHead = document.querySelectorAll('.collapsible-header');
                for (j=0; j < colHead.length; j++) {
                    colHead[j].classList.toggle('dark');
                }

                myMode.classList.toggle('far');
                myMode.classList.toggle('fa-moon');
            }
        }
    } catch (error) {
        console.log("    Error checking theme settings.");
        console.dir(error);
    }
}

// set the flag for whether to try an import /etc/wireguard files
askEtcWireguard = async function(mysudopw) {
    try {
        let mySettings = await Neutralino.filesystem.readFile(storageDir + '/appSettings/appSettings.conf');
        if (mySettings != null && mySettings != "") {
            let settingsObj = JSON.parse(mySettings);
            let askedAlready = settingsObj.import_from_etc_wireguard_asked;
            if (askedAlready == false) {
                askNow(mysudopw);
            } else {
                checkInterfaceStatus(mysudopw, "askEtcWireguard");
            }
        }
    } catch (error) {
        console.log("    ERROR checking whether configurations should be imported from /etc/wireguard:");
        console.dir(error);
    }
}