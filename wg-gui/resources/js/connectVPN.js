connect = function(id){
    try {
        let switchVal = document.getElementById(id);
        if (switchVal.checked == true) {
          let elemId = id.split('|');
          let newId = elemId[1];
          startWg(newId, mysudopw);
        } else {
          let elemId = id.split('|');
          let newId = elemId[1];
          stopWg(newId, mysudopw);
        }
    } catch (error) {
        console.log("    ERROR with connection of VPN: " + error);
    }
};

startWg = async function(interface, sudoPW) {
    try {
        let info = await Neutralino.os.execCommand("echo " + sudoPW + " | sudo -S wg-quick up " + storageDir + "/" + interface + ".conf");
        // console.log(info.stdOut);
        repeatConnCheck(sudoPW, interface);
    } catch (error) {
        console.log("    ERROR trying to bring up the interface: ");
        console.dir(error);
    }  
}

stopWg = async function(interface, sudoPW) {
    try {
        let info = await Neutralino.os.execCommand("echo "+ sudoPW + " | sudo -S wg-quick down " + storageDir + "/" + interface + ".conf");
        // console.log(info.stdOut);
        stopRepeatConnCheck();
    } catch (error) {
        console.log("    ERROR trying to bring down the interface: ");
        console.dir(error);
    }
}