let showToast = function(toastText) {
    // Get the snackbar DIV
    var x = document.getElementById("snackbar");

    // add text for the toast message
    x.innerHTML = toastText
  
    // Add the "show" class to DIV
    x.className = "show";
  
    // After 3 seconds, remove the show class from DIV
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
  }