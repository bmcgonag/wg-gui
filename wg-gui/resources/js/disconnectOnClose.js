disconnectOnClose = async function() {
    // read the settings file and disconnect if true
    // otherwise call app exit.
    let appSettingsStr = await Neutralino.filesystem.readFile(storageDir + '/appSettings/appSettings.conf');
    let appSettings = JSON.parse(appSettingsStr);
    if (appSettings.disconnect_on_close == true) {
        // figure out if any wg connections are active, and if so, disconnect them.
        checkInterfaceStatus(mysudopw, "disconnectOnClose");
    } else {
        exitAppNow();
    }
}

exitAppNow = async function() {
    let appSettingsStr = await Neutralino.filesystem.readFile(storageDir + '/appSettings/appSettings.conf');
    let appSettings = JSON.parse(appSettingsStr);
    if (appSettings.hide_on_close == true) {
        Neutralino.window.hide();
    } else {
        // console.log("Exiting app now.");
        Neutralino.app.exit();
    }
}