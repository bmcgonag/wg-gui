# wg-gui
A Graphical User Interface for Linux based systems to connect to Wireguard Networks.

## Why?
I love a simple gui as much as anyone.  Wireguard ahs had clean GUIs for Windows and MacOS for years now, but nothing really stellar for Linux. I found a couple of projects, but they seemed limited to uuntu versions only. I decided to make something that I'm hoping wiill work on most distributions.

## What does the project depend on?
1. Wireguard
2. Wireguard Tools (e.g. wg-quick)
3. Super User Permissions (this is required in order for Wireguard to make an interface, and destroy it when brought down.)
4. Hopefully, that's all...

## How will I run it?
As AppImage is broken, the Linux files needed are your architecture binary and a resource file.  Download the proper binary from the Releases, as well as the file named resources.neu to your machine, and put them in the same location (e.g. ~/Applications/ or /opt/) then double click on the binary file. You can create a .desktop file as well if you prefer that for your launcher. Different distributions place the .desktop files in different places, but I believe most will look in ~/.local/share/applications/ for any .desktop files you create.

### Creating a .desktop file
Create a new .desktop file in your ~/.local/share/Applications folder called wg-gui.desktop.

`nano ~/.local/share/applications/wg-gui.desktop`

Now paste in the following:

```
[Desktop Entry]
Name=Wg-GUI
Exec=/home/<your user>/applications/wg-cli-linux_x64 %u
Icon=/home/<your user>/applications/logos/wg-cli-logo.png
Comment=Wireguard GUI Client
Type=Application
Terminal=false
Encoding=UTF-8
Categories=Utility;
StartupNotify=true
StartupWMClass=qode
```

Change the `<your user>` in the .desktop snippet above to your actual username.  Additionally, make sure the architecture fits your device.  There are three options.
- wg-cli-linux_x64
- wg-cli-linux_arm64
- wg-cli-linux_armhf

Make sure you have the "resources.neu" file in the same folder (~/.local/share/applications/).

You can get the logo (icon) file with the command:

`wget -O wg-cli-logo-png https://gitlab.com/bmcgonag/wg-gui/-/raw/main/wg-gui/resources/icons/wg-cli-icon.png?ref_type=heads&inline=false`

Make sure to create the "logos" folder in yoru ~/.local/share/applications/ folder, and put the logo in it.

I may script this at some point to make it easier, but for now, it's actually not that difficult to do.

## What's this written in?
HTML, Javascript, JQuery, Materialize-css, Neutrlino.JS

## LICENSE
GNU GPL v3